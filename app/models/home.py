from flask import jsonify
from flask_restful import Resource


class Home(Resource):
    @staticmethod
    def get():
        return jsonify({
            'Home': '/',
            'User': '/users/<id>',
            'UserList': '/users',
            'Question': '/questions/<id>',
            'QuestionList': '/questions',
            'Interest': '/interests/<id>',
            'InterestList': '/interests',
            'Answer': '/answers/<id>',
            'AnswerList': '/answers',
            'Theme': '/themes/<id>',
            'ThemeList': '/themes',
            'UserThemeList': '/users/<userId>/themes',
            'UserTheme': '/users/<userId>/themes/<themeId>',
            'UserAnswerList': '/users/<userId>/answers',
            'UserAnswer': '/users/<userId>/answers/<answerId>',
            'UserQuestionAnswerList': '/users/<userId>/questions/<questionId>/answers',
            'UserInterestThemeList': '/users/<userId>/interests/<interestId>/themes',
        })
