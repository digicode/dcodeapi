from flask import jsonify, request
from flask_restful import Resource
from app.models import DBConnector


class Answer(Resource):
    @staticmethod
    def get(answer_id):
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM answer WHERE answer.id = %s"
                select_data = answer_id
                cursor.execute(select_stmt, select_data)
                for row in cursor:
                    return jsonify(row)
        finally:
            connect.close()


class AnswerList(Resource):
    @staticmethod
    def get():
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM answer"
                cursor.execute(select_stmt)
                answers = [row for row in cursor]
                return answers

        finally:
            connect.close()


class UserAnswerList(Resource):
    @staticmethod
    def get(user_id):
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT a.id id, a.content content, ua.note note FROM useranswers ua " \
                              "JOIN answer a ON ua.answerId = a.id WHERE ua.userId = %s"
                select_data = user_id
                cursor.execute(select_stmt, select_data)
                useranswers = [row for row in cursor]
                return useranswers
        finally:
            connect.close()

    # Used by B-testing
    @staticmethod
    def post(user_id):
        connect = DBConnector.connect_to_db()
        good_answer_id = request.form['answerId']
        question_id = request.form['questionId']
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT answerId FROM questionanswers WHERE questionId = %s"
                select_data = question_id
                cursor.execute(select_stmt, select_data)
                answer_ids = [row['answerId'] for row in cursor if row['answerId'] != int(good_answer_id)]
                [UserAnswer.post(user_id, answerId, note='0') for answerId in answer_ids]
                UserAnswer.post(user_id, good_answer_id, note='5')
            return "La réponse " + good_answer_id + " de la question " + question_id + " notée à 5, le reste à 0"
        finally:
            connect.close()


class UserAnswer(Resource):
    @staticmethod
    def post(user_id, answer_id, **kwargs):
        connect = DBConnector.connect_to_db()
        try:
            request and request.form['note']
        except NameError:
            note = kwargs['note']
        except KeyError:
            note = kwargs['note']
        else:
            note = request.form['note']
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM useranswers WHERE userId = %s AND answerId = %s"
                select_data = (user_id, answer_id)
                exists = cursor.execute(select_stmt, select_data)
                if exists == 0:
                    insert_stmt = "INSERT INTO useranswers VALUES (%s, %s, %s)"
                    insert_data = (user_id, answer_id, note)
                    if cursor.execute(insert_stmt, insert_data) == 1:
                        connect.commit()
                        return "Added in DB"
                    return "Not added"
                else:
                    update_stmt = "UPDATE useranswers SET note = %s WHERE userId = %s AND answerId = %s"
                    update_data = (note, user_id, answer_id)
                    if cursor.execute(update_stmt, update_data) == 1:
                        connect.commit()
                        return "Updated in DB"
                    return "Untouched in DB"
        finally:
            if connect.open:
                connect.close()


class UserQuestionAnswerList(Resource):
    @staticmethod
    def get(user_id, question_id):
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT a.id id, a.content content, ua.note note FROM useranswers ua " \
                              "INNER JOIN answer a ON ua.answerId = a.id " \
                              "INNER JOIN questionanswers qa ON qa.answerId = a.id " \
                              "WHERE ua.userId = %s AND qa.questionId = %s"
                select_data = (user_id, question_id)
                cursor.execute(select_stmt, select_data)
                user_question_answers = [row for row in cursor]
                return user_question_answers
        finally:
            connect.close()
