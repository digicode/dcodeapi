from flask import Flask, json
from flask_restful import Api
from flask_cors import CORS

from app.models.home import Home
from app.models.interest import Interest, InterestList
from app.models.user import User, UserList
from app.models.question import Question, QuestionList
from app.models.answer import Answer, AnswerList, UserAnswerList, UserAnswer, UserQuestionAnswerList
from app.models.theme import Theme, ThemeList, UserThemeList, UserTheme, UserInterestThemeList

app = Flask(__name__)
api = Api(app)
CORS(app)

api.add_resource(Home, '/')
api.add_resource(User, '/users/<id>')
api.add_resource(UserList, '/users')
api.add_resource(Question, '/questions/<id>')
api.add_resource(QuestionList, '/questions')
api.add_resource(Interest, '/interests/<id>')
api.add_resource(InterestList, '/interests')
api.add_resource(Answer, '/answers/<id>')
api.add_resource(AnswerList, '/answers')
api.add_resource(Theme, '/themes/<id>')
api.add_resource(ThemeList, '/themes')
api.add_resource(UserThemeList, '/users/<userId>/themes')
api.add_resource(UserTheme, '/users/<userId>/themes/<themeId>')
api.add_resource(UserAnswerList, '/users/<userId>/answers')
api.add_resource(UserAnswer, '/users/<userId>/answers/<answerId>')
api.add_resource(UserQuestionAnswerList, '/users/<userId>/questions/<questionId>/answers')
api.add_resource(UserInterestThemeList, '/users/<userId>/interests/<interestId>/themes')


if __name__ == '__main__':
    app.run()
