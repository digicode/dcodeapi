from flask_restful import Resource
from app.models import DBConnector


class Question(Resource):
    @staticmethod
    def get(question_id):
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM question WHERE question.id = %s"
                select_data = question_id
                cursor.execute(select_stmt, select_data)
                question = [row for row in cursor][0]
                select_stmt = "SELECT a.id id, a.content content FROM answer a " \
                              "JOIN questionanswers qa on a.id = qa.answerId WHERE qa.questionId = %s"
                select_data = question['id']
                cursor.execute(select_stmt, select_data)
                answers = [row for row in cursor]
                question['answers'] = answers
                return question
        finally:
            connect.close()


class QuestionList(Resource):
    @staticmethod
    def get():
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM question"
                cursor.execute(select_stmt)
                questions = [row for row in cursor]
                return questions

        finally:
            connect.close()
