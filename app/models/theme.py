from flask_restful import Resource
from app.models import DBConnector
from flask import jsonify, request


class Theme(Resource):
    @staticmethod
    def get(theme_id):
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM theme WHERE theme.id = %s"
                select_data = theme_id
                cursor.execute(select_stmt, select_data)
                for row in cursor:
                    return jsonify(row)
        finally:
            connect.close()


class ThemeList(Resource):
    @staticmethod
    def get():
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM theme"
                cursor.execute(select_stmt)
                themes = [row for row in cursor]
                return jsonify(themes)
        finally:
            connect.close()


class UserThemeList(Resource):
    @staticmethod
    def get(user_id):
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT t.id id, t.content content FROM userthemes ut " \
                              "JOIN theme t ON ut.themeId = t.id WHERE ut.userId = %s"
                select_data = user_id
                cursor.execute(select_stmt, select_data)
                userthemes = [row for row in cursor]
                return userthemes
        finally:
            connect.close()

    @staticmethod
    def post(user_id):
        connect = DBConnector.connect_to_db()
        if request.form['interested'] == "true" or request.form['interested'] == 1:
            interested = True
        else:
            interested = False
        interest_id = request.form['interest_id']
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT themeId FROM interestthemes WHERE interestId = %s"
                select_data = interest_id
                cursor.execute(select_stmt, select_data)
                theme_ids = [row['themeId'] for row in cursor]
                [UserTheme.post(user_id, themeId, interested=interested) for themeId in theme_ids]
            return "All updated in DB"
        finally:
            connect.close()


class UserTheme(Resource):
    @staticmethod
    def post(user_id, theme_id, **kwargs):
        connect = DBConnector.connect_to_db()
        try:
            request and request.form['interested']
        except NameError:
            interested = kwargs['interested']
        except KeyError:
            interested = kwargs['interested']
        else:
            if request.form['interested'] == "true" or request.form['interested'] == 1:
                interested = True
            else:
                interested = False
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM userthemes WHERE userId = %s AND themeId = %s"
                select_data = (user_id, theme_id)
                exists = cursor.execute(select_stmt, select_data)
                if exists == 0:
                    insert_stmt = "INSERT INTO userthemes VALUES (%s, %s, %s)"
                    insert_data = (user_id, theme_id, interested)
                    if cursor.execute(insert_stmt, insert_data) == 1:
                        connect.commit()
                        return "Added in DB"
                    else:
                        return "Couldn't add in DB"
                else:
                    update_stmt = "UPDATE userthemes SET interested = %s WHERE userid = %s AND themeId = %s"
                    update_data = (interested, user_id, theme_id)
                    if cursor.execute(update_stmt, update_data) == 1:
                        connect.commit()
                        return "Updated in DB"
                return "Already in DB"
        finally:
            connect.close()

    @staticmethod
    def delete(user_id, theme_id):
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                delete_stmt = "DELETE FROM userthemes WHERE userID = %s AND themeid = %s"
                delete_data = (user_id, theme_id)
                deleted = cursor.execute(delete_stmt, delete_data)
                if deleted == 1:
                    connect.commit()
                    return "Deleted from DB"
                return "Didn't exist in DB"
        finally:
            connect.close()


class UserInterestThemeList(Resource):
    @staticmethod
    def get(user_id, interest_id):
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT t.id id, t.content content, ut.interested interested FROM userthemes ut " \
                              "INNER JOIN theme t ON ut.themeId = t.id " \
                              "INNER JOIN interestthemes it ON it.themeId = t.id " \
                              "WHERE ut.userId = %s AND it.interestId = %s"
                select_data = (user_id, interest_id)
                cursor.execute(select_stmt, select_data)
                userquestionanswers = [row for row in cursor]
                return userquestionanswers
        finally:
            connect.close()
