from flask_restful import Resource
from app.models import DBConnector
from flask import jsonify


class User(Resource):
    @staticmethod
    def get(user_id):
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM user WHERE user.id = %s"
                select_data = user_id
                cursor.execute(select_stmt, select_data)
                for row in cursor:
                    return jsonify(row)
        finally:
            connect.close()


class UserList(Resource):
    @staticmethod
    def get():
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM user"
                cursor.execute(select_stmt)
                users = [row for row in cursor]
                return users

        finally:
            connect.close()
