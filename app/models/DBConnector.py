import pymysql.cursors


def connect_to_db():
    connect = pymysql.connect(host='localhost',
                              user='root',
                              password='',
                              db='digitalcode',
                              cursorclass=pymysql.cursors.DictCursor)
    return connect
