from flask_restful import Resource
from app.models import DBConnector


class Interest(Resource):
    @staticmethod
    def get(interest_id):
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM interest WHERE interest.id = %s"
                select_data = interest_id
                cursor.execute(select_stmt, select_data)
                interest = [row for row in cursor][0]
                select_stmt = "SELECT t.id id, t.content content FROM theme t " \
                              "JOIN interestthemes it on t.id = it.themeId WHERE it.interestId = %s"
                select_data = interest['id']
                cursor.execute(select_stmt, select_data)
                themes = [row for row in cursor]
                interest['themes'] = themes
                return interest
        finally:
            connect.close()


class InterestList(Resource):
    @staticmethod
    def get():
        connect = DBConnector.connect_to_db()
        try:
            with connect.cursor() as cursor:
                select_stmt = "SELECT * FROM interest"
                cursor.execute(select_stmt)
                interests = [row for row in cursor]
                return interests

        finally:
            connect.close()
