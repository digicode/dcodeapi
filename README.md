# DCodeAPI

Python Web Service

####Windows
Clone this in a local folder, then go into the cloned folder named DCodeAPI.

In this folder, open a terminal and run **py -3 -m venv venv** (assuming you have Python 3.* and virtualenv installed).

Next, activate it using **venv\Scripts\activate**

Then, install the packages needed by running **pip install _package_name_** for the following packages :

- Flask
- flask_RESTful
- flask_CORS
- pymysql

Last, set the environnment variable FLASK_APP with **set FLASK_APP=app/app.py**

You can now run the app with **flask run**